/* eslint-disable prettier/prettier */
import {
  Dimensions,
  Image,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import React from 'react';
import {ImageHeader, Logo} from '../../assets';
import {Balance, ActiveOrder, ButtonIcon} from '../../components';
import {WARNA_ABU_ABU} from '../../utils/constant';

const Home = () => {
  return (
    <>
      <View style={styles.home}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground source={ImageHeader} style={styles.header}>
            <Image source={Logo} style={styles.logo} />
            <View style={styles.hello}>
              <Text style={styles.selamatDatang}>Selamat Datang</Text>
              <Text style={styles.username}>Arif yudi</Text>
            </View>
          </ImageBackground>
          <Balance />
          <View style={styles.service}>
            <Text style={styles.label}>Layanan Kami</Text>
            <View style={styles.iconService}>
              <ButtonIcon type="layanan" title="Kiloan" />
              <ButtonIcon type="layanan" title="Satuan" />
              <ButtonIcon type="layanan" title="VIP" />
              <ButtonIcon type="layanan" title="Karpet" />
              <ButtonIcon type="layanan" title="Setrika" />
              <ButtonIcon type="layanan" title="Ekspress" />
            </View>
          </View>
          <View style={styles.ActiveOrder}>
            <Text style={styles.label}>Pesanan Aktif</Text>
            <ActiveOrder title="Pesanan No. 0002142" status="Sudah Selesai" />
            <ActiveOrder title="Pesanan No. 0002142" status="Masih Dicuci" />
            <ActiveOrder title="Pesanan No. 0002142" status="Sudah Selesai" />
            <ActiveOrder title="Pesanan No. 0002142" status="Sudah Selesai" />
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  home: {
    flex: 1,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {marginTop: windowHeight * 0.025},
  selamatDatang: {fontSize: 24, fontFamily: 'TitilliumWeb-Regular'},
  username: {fontSize: 18, fontFamily: 'TitilliumWeb-Bold'},
  service: {
    paddingLeft: 30,
    paddingTop: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
  },
  iconService: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    flexWrap: 'wrap',
  },
  ActiveOrder: {
    paddingTop: 10,
    paddingHorizontal: 30,
    backgroundColor: WARNA_ABU_ABU,
    flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
});
