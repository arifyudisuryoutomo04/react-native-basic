import Home from './Home';
import Account from './Account';
import Orders from './Orders';
import Splash from './Splash';

export {Splash, Home, Account, Orders};
