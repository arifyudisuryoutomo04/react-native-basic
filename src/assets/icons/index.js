/* eslint-disable prettier/prettier */
import IconAkun from './akun.svg';
import IconAkunActive from './akunActive.svg';
import IconHome from './home.svg';
import IconHomeActive from './homeActive.svg';
import IconPesanan from './pesanan.svg';
import IconActiveOrder from './ActiveOrder.svg';
import IconAddSaldo from './addSaldo.svg';
import IconGetPoint from './getPoint.svg';
import IconKiloan from './kiloan.svg';
import IconVIP from './vip.svg';
import IconSatuan from './satuan.svg';
import IconEkspress from './ekspress.svg';
import IconSetrika from './setrika.svg';
import IconKarpet from './karpet.svg';
import IconPesananAktif from './pesananAktif.svg';

export {
  IconAkun,
  IconAkunActive,
  IconHome,
  IconHomeActive,
  IconPesanan,
  IconActiveOrder,
  IconAddSaldo,
  IconGetPoint,
  IconKiloan,
  IconVIP,
  IconSatuan,
  IconEkspress,
  IconSetrika,
  IconKarpet,
  IconPesananAktif,
};
