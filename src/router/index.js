/* eslint-disable prettier/prettier */
import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Home, Splash, Orders, Account} from '../pages';
import {BottomNavigation} from '../components';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <>
      <Tab.Navigator tabBar={props => <BottomNavigation {...props} />}>
        <Tab.Screen
          options={{headerShown: false}}
          name="Home"
          component={Home}
        />
        <Tab.Screen
          options={{headerShown: false}}
          name="Orders"
          component={Orders}
        />
        <Tab.Screen
          options={{headerShown: false}}
          name="Account"
          component={Account}
        />
      </Tab.Navigator>
    </>
  );
};

const Router = () => {
  return (
    <>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MainApp"
          component={MainApp}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </>
  );
};

export default Router;

const styles = StyleSheet.create({});
